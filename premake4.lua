solution "Luaflow"
   configurations { "debug", "release" }
   configuration "debug"
      defines { "DEBUG" }
      flags { "Symbols" }
   configuration "release"
      defines { "NDEBUG" }
      flags { "OptimizeSize" }
   configuration { "gmake" }
      buildoptions { "-std=c11", "-Wall", "-Wextra" }
   configuration { "release", "gmake" }
      buildoptions { "-Werror" }

   project "luaflow"
      kind "ConsoleApp"
      language "C"
      files { "src/**.c" }
      links { "lua", "m", "openflow", "dl" }
      includedirs { "deps/lua-5.2.1/src", "deps/openflow/include",
                    "deps/openflow/lib" }
      libdirs { "deps/lua-5.2.1/src", "deps/openflow/lib" }
