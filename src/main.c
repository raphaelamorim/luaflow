#define _BSD_SOURCE
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdlib.h>
#include <inttypes.h>

#define HAVE_STRLCPY 1

#include "dbg.h"
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
#include "ofp-print.h"
#include "ofpbuf.h"
#include "openflow/openflow.h"
#include "poll-loop.h"
#include "rconn.h"
#include "timeval.h"
#include "vconn.h"
#include "flow.h"
#include "packets.h"

#define MAX_SWITCHES 10

/* crap for operationssystem that doesn't provide us 64 bit ops */
#ifndef be64toh
#if BYTE_ORDER == LITTLE_ENDIAN
static inline uint64_t
ydr_swap64(uint64_t x)
{
#define LT(n) n##ULL
   return 
      ((LT(0x00000000000000ff) & x) << 56) | 
      ((LT(0x000000000000ff00) & x) << 40) | 
      ((LT(0x0000000000ff0000) & x) << 24) | 
      ((LT(0x00000000ff000000) & x) << 8) | 
      ((LT(0x000000ff00000000) & x) >> 8) | 
      ((LT(0x0000ff0000000000) & x) >> 24) | 
      ((LT(0x00ff000000000000) & x) >> 40) | 
      ((LT(0xff00000000000000) & x) >> 56) ; 
#undef LT
}

#define be64toh(x) ydr_swap64((x))
#define htobe64(x) ydr_swap64((x))
#endif /* BYTE_ORDER */
#if BYTE_ORDER == BIG_ENDIAN
#define be64toh(x) (x)
#define htobe64(x) (x)
#endif /* BYTE_ORDER */
#endif /* be64toh */

struct lua_State *L;
struct pvconn *pvconn;
bool running;

struct ofsw {
    uint64_t dpid;
    struct rconn *rconn;
    enum {
        HANDSHAKING,
        READY,
    } state;
} switches[MAX_SWITCHES];
int no_switches = 0;

static struct ofsw *find_switch(uint64_t dpid)
{
    int i;

    for (i = 0; i < no_switches; ++i)
    {
        if (dpid == switches[i].dpid)
            return &switches[i];
    }

    return NULL;
}

static bool ofsw_recv_echo_request(struct ofsw *sw, struct ofpbuf *buf)
{
    struct ofpbuf *rbuf;
    int ret;

    /* debugsw(sw, "Echo request received!"); */
    rbuf = ofpbuf_clone(buf);
    check_mem(rbuf);
    ((struct ofp_header *)rbuf->data)->type = OFPT_ECHO_REPLY;
    ret = rconn_send(sw->rconn, rbuf, NULL);
    check(ret == 0, "Error sending message to switch.");

    return true;

error:
    return false;
}

static bool lf_pushphyport(lua_State *L, const struct ofp_phy_port *port)
{
    lua_newtable(L);
    lua_pushstring(L, "port_no");
    lua_pushinteger(L, ntohs(port->port_no));
    lua_settable(L, -3);
    lua_pushstring(L, "hw_addr");
    {
        char mac[18];
        snprintf(mac, 18, "%02x:%02x:%02x:%02x:%02x:%02x",
                 port->hw_addr[0], port->hw_addr[1],
                 port->hw_addr[2], port->hw_addr[3],
                 port->hw_addr[4], port->hw_addr[5]);
        lua_pushstring(L, mac);
    }
    lua_settable(L, -3);
    lua_pushstring(L, "name");
    lua_pushstring(L, port->name);
    lua_settable(L, -3);
    lua_pushstring(L, "config");
    lua_pushinteger(L, ntohl(port->config));
    lua_settable(L, -3);
    lua_pushstring(L, "state");
    lua_pushinteger(L, ntohl(port->state));
    lua_settable(L, -3);
    lua_pushstring(L, "curr");
    lua_pushinteger(L, ntohl(port->curr));
    lua_settable(L, -3);
    lua_pushstring(L, "advertised");
    lua_pushinteger(L, ntohl(port->advertised));
    lua_settable(L, -3);
    lua_pushstring(L, "supported");
    lua_pushinteger(L, ntohl(port->supported));
    lua_settable(L, -3);
    lua_pushstring(L, "peer");
    lua_pushinteger(L, ntohl(port->peer));
    lua_settable(L, -3);

    return true;
}

static bool lf_pushfeatures(lua_State *L,
                            const struct ofp_switch_features *features)
{
    lua_newtable(L);
    lua_pushstring(L, "datapath_id");
    {
        char s[17];
        snprintf(s, 17, "%016"PRIx64, (uint64_t)be64toh(features->datapath_id));
        lua_pushstring(L, s);
    }
    lua_settable(L, -3);
    lua_pushstring(L, "n_buffers");
    lua_pushnumber(L, ntohl(features->n_buffers));
    lua_settable(L, -3);
    lua_pushstring(L, "n_tables");
    lua_pushnumber(L, ntohl(features->n_tables));
    lua_settable(L, -3);
    lua_pushstring(L, "capabilities");
    lua_pushnumber(L, ntohl(features->capabilities));
    lua_settable(L, -3);
    lua_pushstring(L, "actions");
    lua_pushnumber(L, ntohl(features->actions));
    lua_settable(L, -3);
    lua_pushstring(L, "ports");
    {
        int no_ports, i;
        no_ports = (ntohs(features->header.length) -
                    sizeof(struct ofp_switch_features)) /
            sizeof(struct ofp_phy_port);
        lua_createtable(L, no_ports, 0);
        for (i = 0; i < no_ports; ++i) {
            lua_pushinteger(L, i+1);
            lf_pushphyport(L, &features->ports[i]);
            lua_settable(L, -3);
        }
    }
    lua_settable(L, -3);

    return true;
}

static int lf_flow_index(lua_State *L)
{
    struct flow *flow;
    const char *key;

    flow = luaL_checkudata(L, 1, "lf_flow");
    key = luaL_checkstring(L, 2);

    if (!strcmp("nw_src", key))
        lua_pushunsigned(L, ntohl(flow->nw_src));
    else if (!strcmp("nw_dst", key))
        lua_pushunsigned(L, ntohl(flow->nw_dst));
    else if (!strcmp("in_port", key))
        lua_pushunsigned(L, ntohs(flow->in_port));
    else if (!strcmp("dl_vlan", key))
        lua_pushunsigned(L, ntohs(flow->dl_vlan));
    else if (!strcmp("dl_type", key))
    {
        uint32_t eth_type = ntohs(flow->dl_type);
        switch (eth_type) {
        case ETH_TYPE_IP:
            lua_pushstring(L, "ip");
            break;
        case ETH_TYPE_ARP:
            lua_pushstring(L, "arp");
            break;
        default:
            lua_pushunsigned(L, eth_type);
            break;
        }
    }
    else if (!strcmp("tp_src", key))
        lua_pushunsigned(L, ntohs(flow->tp_src));
    else if (!strcmp("tp_dst", key))
        lua_pushunsigned(L, ntohs(flow->tp_dst));
    else if (!strcmp("dl_src", key))
    {
        char dl_src[18];
        snprintf(dl_src, sizeof(dl_src), ETH_ADDR_FMT,
                 ETH_ADDR_ARGS(flow->dl_src));
        lua_pushstring(L, dl_src);
    }
    else if (!strcmp("dl_dst", key))
    {
        char dl_dst[18];
        snprintf(dl_dst, sizeof(dl_dst), ETH_ADDR_FMT,
                 ETH_ADDR_ARGS(flow->dl_dst));
        lua_pushstring(L, dl_dst);
    }
    else if (!strcmp("dl_vlan_pcp", key))
        lua_pushunsigned(L, flow->dl_vlan_pcp);
    else if (!strcmp("nw_tos", key))
        lua_pushunsigned(L, flow->nw_tos);
    else if (!strcmp("nw_proto", key))
    {
        if (ntohs(flow->dl_type) == ETH_TYPE_IP)
            switch (flow->nw_proto) {
            case IP_TYPE_ICMP:
                lua_pushstring(L, "icmp");
                break;
            case IP_TYPE_TCP:
                lua_pushstring(L, "tcp");
                break;
            case IP_TYPE_UDP:
                lua_pushstring(L, "udp");
                break;
            default:
                lua_pushunsigned(L, flow->nw_proto);
                break;
            }
        else
            lua_pushunsigned(L, flow->nw_proto);
    }
    else
        lua_pushnil(L);

    return 1;
}

static bool lf_pushflow(lua_State *L, struct ofpbuf *data, uint16_t in_port)
{
    struct flow *flow;

    flow = lua_newuserdata(L, sizeof(struct flow));
    flow_extract(data, in_port, flow);
    if (luaL_newmetatable(L, "lf_flow"))
    {
        lua_pushstring(L, "__index");
        lua_pushcfunction(L, lf_flow_index);
        lua_settable(L, -3);
    }
    lua_setmetatable(L, -2);

    return true;
}

static bool ofsw_recv_features_reply(struct ofsw *sw, struct ofpbuf *buf)
{
    struct ofp_switch_features *r;

    debugsw(sw, "Features reply received!");
    r = buf->data;
    if (sw->state == HANDSHAKING) {
        sw->dpid = be64toh(r->datapath_id);
        sw->state = READY;
        debugsw(sw, "State changed to READY!");

        lua_getglobal(L, "switch_ready");
        {
             char s[17];
             snprintf(s, 17, "%016"PRIx64, sw->dpid);
             lua_pushstring(L, s);
        }
        lf_pushfeatures(L, r);
        if (lua_pcall(L, 2, 0, 0) != 0) {
            log_err("Error calling lua callback: %s", lua_tostring(L, -1));
            lua_pop(L, 1);
        }
    }

    return true;
}

static bool ofsw_recv_packet_in(struct ofsw *sw, struct ofpbuf *buf)
{
     struct ofp_packet_in *packetin;
     struct ofpbuf pkt_data;

     debugsw(sw, "PacketIn received!");
     packetin = buf->data;
     lua_getglobal(L, "packet_in");
     {
          char s[17];
          snprintf(s, 17, "%016"PRIx64, sw->dpid);
          lua_pushstring(L, s);
     }
     {
         uint32_t buffer_id;
         buffer_id = ntohl(packetin->buffer_id);
         if (buffer_id == (uint32_t)-1)
             lua_pushnil(L);
         else
             lua_pushunsigned(L, buffer_id);
     }
     pkt_data.data = packetin->data;
     pkt_data.size = ntohs(packetin->header.length) - offsetof(struct ofp_packet_in, data);
     lf_pushflow(L, &pkt_data, ntohs(packetin->in_port));
     
     if (lua_pcall(L, 3, 0, 0) != 0) {
          log_err("Error calling lua callback: %s", lua_tostring(L, -1));
          lua_pop(L, 1);
     }

     return true;
}

static bool ofsw_recv_default(struct ofsw *sw, struct ofpbuf *buf)
{
    char *s;
    int i;

    s = ofp_to_string(buf->data, buf->size, 0);
    check_mem(s);
    for (i = 0; s[i] != '\n' && s[i] != '\0'; ++i);
    s[i] = '\0';
    log_info("[dpid=0x%"PRIx64"] Unhandled openflow message: %s", sw->dpid, s);
    free(s);

    return true;

error:
    return false;
}

bool ofsw_recv(struct ofsw *sw)
{
    struct ofpbuf *buf;
    bool ret;

    buf = rconn_recv(sw->rconn);
    if (buf == NULL)
        return true;
    switch (((struct ofp_header *)buf->data)->type) {
    case OFPT_ECHO_REQUEST:
        ret = ofsw_recv_echo_request(sw, buf);
        break;
    case OFPT_FEATURES_REPLY:
        ret = ofsw_recv_features_reply(sw, buf);
        break;
    case OFPT_PACKET_IN:
         ret = ofsw_recv_packet_in(sw, buf);
         break;
    default:
        ret = ofsw_recv_default(sw, buf);
        break;
    }
    ofpbuf_delete(buf);
    check(ret, "Error processing message from switch.");

    return true;

error:
    return false;
}

bool ofsw_wait(struct ofsw *sw)
{
    rconn_run_wait(sw->rconn);
    rconn_recv_wait(sw->rconn);
    return true;
}

bool ofsw_run(struct ofsw *sw)
{
    ofsw_recv(sw); // TODO Try to receive more messages by time
    rconn_run(sw->rconn);
    return true;
}

int lf_packet_out(lua_State *L)
{
    const char *dpid_str;
    uint32_t buffer_id;
    uint16_t in_port, out_port;
    struct ofpbuf *ofpbuf;
    struct ofsw *sw;

    dpid_str = luaL_checkstring(L, 1);
    in_port = luaL_checkunsigned(L, 2);
    buffer_id = luaL_checkunsigned(L, 3);
    if (lua_isstring(L, 4) && !strcmp("all", lua_tostring(L, 4)))
        out_port = OFPP_ALL;
    else
        out_port = luaL_checkunsigned(L, 4);

    {
        uint64_t dpid;
        sscanf(dpid_str, "%"SCNx64, &dpid);
        sw = find_switch(dpid);
    }
    if (sw == NULL) {
        lua_pushstring(L, "switch not found");
        lua_error(L);
    }

    ofpbuf = make_buffered_packet_out(buffer_id, in_port, out_port);
    rconn_send(sw->rconn, ofpbuf, NULL);

    debug("Packet out");

    return 0;
}

int lf_add_simple_flow(lua_State *L)
{
    const char *dpid_str;
    uint32_t buffer_id;
    uint16_t out_port, idle_timeout;
    struct flow *flow;
    struct ofpbuf *ofpbuf;
    struct ofsw *sw;

    dpid_str = luaL_checkstring(L, 1);
    flow = luaL_checkudata(L, 2, "lf_flow");
    buffer_id = luaL_checkunsigned(L, 3);
    if (lua_isstring(L, 4) && !strcmp("all", lua_tostring(L, 4)))
        out_port = OFPP_ALL;
    else
        out_port = luaL_checkunsigned(L, 4);
    idle_timeout = luaL_checkunsigned(L, 5);

    {
        uint64_t dpid;
        sscanf(dpid_str, "%"SCNx64, &dpid);
        sw = find_switch(dpid);
    }
    if (sw == NULL) {
        lua_pushstring(L, "switch not found");
        lua_error(L);
    }

    ofpbuf = make_add_simple_flow(flow, buffer_id, out_port, idle_timeout);
    // ofp_print(stderr, ofpbuf->data, ofpbuf->size, 0);
    rconn_send(sw->rconn, ofpbuf, NULL);

    return 0;
}

int lf_packet_drop(lua_State *L)
{
    const char *dpid_str;
    uint32_t buffer_id;
    uint16_t in_port;
    struct ofsw *sw;
    struct ofp_packet_out *packetout;
    struct ofpbuf *ofpbuf;

    dpid_str = luaL_checkstring(L, 1);
    in_port = luaL_checkunsigned(L, 2);
    buffer_id = luaL_checkunsigned(L, 3);

    {
        uint64_t dpid;
        sscanf(dpid_str, "%"SCNx64, &dpid);
        sw = find_switch(dpid);
    }
    if (sw == NULL) {
        lua_pushstring(L, "switch not found");
        lua_error(L);
    }

    packetout = make_openflow(sizeof(struct ofp_packet_out), OFPT_PACKET_OUT,
                              &ofpbuf);
    packetout->buffer_id = htonl(buffer_id);
    packetout->in_port = htons(in_port);
    packetout->actions_len = htons(0);
    rconn_send(sw->rconn, ofpbuf, NULL);

    debug("Packet drop");

    return 0;
}

int lf_add_drop_flow(lua_State *L)
{
    const char *dpid_str;
    uint32_t buffer_id;
    uint16_t idle_timeout;
    struct flow *flow;
    struct ofpbuf *ofpbuf;
    struct ofsw *sw;

    dpid_str = luaL_checkstring(L, 1);
    flow = luaL_checkudata(L, 2, "lf_flow");
    buffer_id = luaL_checkunsigned(L, 3);
    idle_timeout = luaL_checkunsigned(L, 4);

    {
        uint64_t dpid;
        sscanf(dpid_str, "%"SCNx64, &dpid);
        sw = find_switch(dpid);
    }
    if (sw == NULL) {
        lua_pushstring(L, "switch not found");
        lua_error(L);
    }

    ofpbuf = make_add_flow(flow, buffer_id, idle_timeout, 0);
    rconn_send(sw->rconn, ofpbuf, NULL);

    return 0;
}

int main(int argc, char *argv[])
{
    int ret;

    time_init();

    if (argc != 2) {
        printf("Usage: %s <lua_script.lua>\n", argv[0]);
        return 0;
    }

    L = luaL_newstate();
    check(L != NULL, "Error creating lua state.");
    luaL_openlibs(L);
    ret = luaL_dofile(L, argv[1]);
    check(!ret, "Error loading lua script.");
    lua_pushcfunction(L, lf_packet_out);
    lua_setglobal(L, "packet_out");
    lua_pushcfunction(L, lf_add_simple_flow);
    lua_setglobal(L, "add_simple_flow");
    lua_pushcfunction(L, lf_packet_drop);
    lua_setglobal(L, "packet_drop");
    lua_pushcfunction(L, lf_add_drop_flow);
    lua_setglobal(L, "add_drop_flow");

    pvconn_open("ptcp:", &pvconn);

    running = true;
    do {
        struct vconn *new_vconn;
        int i;

        pvconn_wait(pvconn);
        poll_block();

        ret = pvconn_accept(pvconn, OFP_VERSION, &new_vconn);
        if (!ret || ret == EAGAIN) {
            if (!ret) {
                struct ofpbuf *b;
                struct ofsw *sw;

                sw = &switches[no_switches++];
                sw->state = HANDSHAKING;
                sw->rconn = rconn_new_from_vconn("tcp", new_vconn);
                log_info("New switch connected");
                make_openflow(sizeof(struct ofp_header), OFPT_FEATURES_REQUEST, &b);
                rconn_send(sw->rconn, b, NULL);
            }
        } else {
            pvconn_close(pvconn);
            running = false;
        }

        for (i = 0; i < no_switches; ++i) {
            struct ofsw *sw;

            sw = &switches[i];
            ofsw_run(sw);
            ofsw_wait(sw);
        }
    } while (running);

    return 0;

error:
    return -1;
}
