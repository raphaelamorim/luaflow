Dijkstra = { 
              graph = {},
              predecessor = {}        
           }

function Dijkstra:new (o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function Dijkstra:solve(source, sink)
    s = {}
    local Q = self.graph:get_all_nodes()
    self:initializeSingleSource(source, Q)
    
    while (next(Q) ~= nil) do
        u = self:extractMin(Q);
        table.insert(s, u)
        
        for k,v in pairs(self.graph:adjacencyList(u)) do
            self:relax(u, v);
        end
    end

    return self:extractPath(source, sink);
end

function Dijkstra:initializeSingleSource(vertex, Q)
    for node_name, node in pairs(Q) do
        node.weight = math.huge
        self.predecessor[node] = node
    end
    vertex.weight = 0.0
end

function Dijkstra:extractMin(queue)
    local min = {weight = math.huge}   
    local count = 0
    for k,v in pairs(queue) do
        if(v.weight < min.weight) then
            min = v
            queue[k] = nil
        end
        count = count + 1
    end
    return min
end

function Dijkstra:relax(u, v)
    weight = self.graph:edge_weight(u, v)
    if (v.weight > u.weight + weight) then
        v.weight = u.weight + weight
        self.predecessor[v] = u
    end
end

function Dijkstra:extractPath(source, sink) 
    path = {}
    if (self:recurrency(path, source, sink)) then
        table.insert(path, sink)
        return path
    else
        return path
    end
end

function Dijkstra:recurrency(path, source, sink)
    local current = self.predecessor[sink]
    if (current.alias ~= sink.alias) then
        if(self:recurrency(path, source, current)) then
            table.insert(path, current)
        elseif (current.alias ~= source.alias) then
            return false
        else
            return true
        end
    end
    return true
end


