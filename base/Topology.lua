require "Host"
require "Switch"
require "Link"
require "Port"
require "Dijkstra"

Topology = { 
              name = "",
              nodes = {},
              links = {}          
           }

function Topology:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self 
  return o
end

function Topology:load_config(filename)
    dofile(filename)
    
    --load switches
    for switch_name, switch_attributes in pairs(Switches) do
        switch_attributes.alias = switch_name
        self.nodes[switch_name] = Switch:new(switch_attributes)
    end    
    --load hosts
    for host_name,host_attributes in pairs(Hosts) do
        host_attributes.alias = host_name
        self.nodes[host_name] = Host:new(host_attributes)
    end
    --creating links
    for src, dst in pairs(Connection) do
        local edge = {}
        endpoint1 = ""
        endpoint2 = ""
        
        if(Hosts[src] ~= nil) then
            --insert endpoints
            edge[src] = self.nodes[src]
            endpoint1 = ""..src
            for switch_name,port_number in pairs(dst) do
                edge[switch_name] = self.nodes[switch_name].ports[port_number]
                endpoint2 = ""..switch_name
            end            
        elseif (next(dst) ~= nil) then
            --for each port in the source switch
            for port_number, destination in pairs(Connection[src]) do
                --insert endpoints
                edge[src] = self.nodes[src].ports[port_number]
                endpoint1 = ""..src
                for dst_name, dst_port in pairs(destination) do
                    if(dst_port == nil) then
                        edge[dst_name] = self.nodes[dst_name]
                    else
                        edge[dst_name] = self.nodes[dst_name].ports[dst_port]
                    end
                    endpoint2 = ""..dst_name
                end 
            end 
        end
        
        if(next(dst) ~= nil) or (Hosts[src] ~= nil) then
            --adding link to the adjancency table
            local link = Link:new{endpoints=edge}
            
            if(self.links[endpoint1] == nil) then
                 self.links[endpoint1] = {}
            end
            
            if(self.links[endpoint2] == nil) then
                 self.links[endpoint2] = {}
            end
                                                
            self.links[endpoint1][endpoint2] = link
            self.links[endpoint2][endpoint1] = link
        end
    end  
end

--returns optimal route from source to the destination
function Topology:get_route(src,dst)
   local src_node = nil
   local dst_node = nil

   if(src.type == "host") or (src.type == "switch") then
       src_node = src
   else
       src_node = self.nodes[""..src]
   end

   if(dst.type == "host") or (dst.type == "switch") then
       dst_node = dst
   else
       dst_node = self.nodes[""..dst]
   end
   
   pathSolver = Dijkstra:new{graph = self}
   return pathSolver:solve(src_node, dst_node)
end

--to be used by the routing algorithm to optimize the route
--between two nodes u and v
function Topology:edge_weight(u,v)
   local link = self.links[u.alias][v.alias]
   return link.total_bandwidth - link.free_bandwidth
end

function Topology:adjacencyList(u)
    local list = {}
    for k,v in pairs(self.links[u.alias]) do
        list[k] = self.nodes[k]
    end
    return list
end

function Topology:get_all_nodes()
    local node_list = {}
    for k,v in pairs(self.nodes) do
        node_list[k] = v
    end
    return node_list
end