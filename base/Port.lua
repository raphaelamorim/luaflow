Port = { 
            number = "",
            switch = "",
            link = "",
            speed = 1000,
            flows = {},
            status = "off",
            type = "port"
        }

function Port:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end