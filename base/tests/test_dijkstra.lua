package.path = package.path .. ";../?.lua" 
require "Topology"

myTopology = Topology:new{name = "mininet"}
myTopology:load_config("../custom_topology_config.lua")

route = myTopology:get_route("host1", "host2")

print("#1 Route host1 -> host2")

strRoute = ""

for i,v in ipairs(route) do
    if(v.type == "switch") then
        link = myTopology.links[route[i-1].alias][route[i].alias]        
        port_in = link.endpoints[route[i].alias]
        
        link = myTopology.links[route[i].alias][route[i+1].alias]
        port_out = link.endpoints[route[i].alias]
        
        strRoute = strRoute .. "("..port_in.number ..") "
        strRoute = strRoute .. v.alias .. " (".. port_out.number ..") -> "
    else
        if(strRoute == "") then
            strRoute = strRoute .. v.alias .. "->"
        else
            strRoute = strRoute .. v.alias
        end
    end
end

print(strRoute)

route = myTopology:get_route("host1", "host3")

print("#2 Route host1 -> host3")

strRoute = ""

for i,v in ipairs(route) do
    if(v.type == "switch") then
        link = myTopology.links[route[i-1].alias][route[i].alias]        
        port_in = link.endpoints[route[i].alias]
        
        link = myTopology.links[route[i].alias][route[i+1].alias]
        port_out = link.endpoints[route[i].alias]
        
        strRoute = strRoute .. "("..port_in.number ..") "
        strRoute = strRoute .. v.alias .. " (".. port_out.number ..") -> "
    else
        if(strRoute == "") then
            strRoute = strRoute .. v.alias .. "->"
        else
            strRoute = strRoute .. v.alias
        end
    end
end

print(strRoute)

route = myTopology:get_route("host1", "host4")

print("#3 Route host1 -> host4")

strRoute = ""

for i,v in ipairs(route) do
    if(v.type == "switch") then
        link = myTopology.links[route[i-1].alias][route[i].alias]        
        port_in = link.endpoints[route[i].alias]
        
        link = myTopology.links[route[i].alias][route[i+1].alias]
        port_out = link.endpoints[route[i].alias]
        
        strRoute = strRoute .. "("..port_in.number ..") "
        strRoute = strRoute .. v.alias .. " (".. port_out.number ..") -> "
    else
        if(strRoute == "") then
            strRoute = strRoute .. v.alias .. "->"
        else
            strRoute = strRoute .. v.alias
        end
    end
end

print(strRoute)