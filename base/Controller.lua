require "Topology"

myTopology = Topology:new{name = "mininet"}
myTopology:load_config("custom_topology_config.lua")

function switch_ready(dpid, features)
    print(">> New switch connected: " .. dpid)
    --TODO
    --Insert switch features into switch objects
end

function packet_in(dpid, msg)
  
  print(">> New packet received from " .. dpid)

  --TODO
  --Route packets using Dijkstra algorithm implementation
  --ex: route = myTopology:getRoute("host1, host2") 
  --    Send flowmod messages to all switches in the route

  packet_out(dpid, { buffer_id = msg.buffer_id,
                     data = msg.data,
                     actions = {{ type = "output", port = "all"}} })
end