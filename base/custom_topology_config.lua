require "base_config"

-- Switches: switch = {datapath_id = dpid, ...}

switches{
switch1 = {datapath_id = "00:00:00:00:00:00:00:01"},
switch2 = {datapath_id = "00:00:00:00:00:00:00:02"},
switch3 = {datapath_id = "00:00:00:00:00:00:00:03"},
switch4 = {datapath_id = "00:00:00:00:00:00:00:04"}
}

-- Hosts: host = {mac = mac_addr, ...}

hosts{
host1 = {mac = "01:00:00:00:00:00:00:03"},
host2 = {mac = "01:00:00:00:00:00:00:04"},
host3 = {mac = "01:00:00:00:00:00:00:05"},
host4 = {mac = "01:00:00:00:00:00:00:06"},
}

-- Connections: Connection.switch[port#] = {switch=port#} or
--              Connection.switch[port#] = {host} or
--              Connection.host = {switch=port#}

Connection.host1       = { switch1 = 2}
Connection.host2       = { switch2 = 2}
Connection.host3       = { switch4 = 2}
Connection.host4       = { switch3 = 3}
Connection.switch1[1]  = { switch2 = 1} 
Connection.switch2[3]  = { switch3 = 1}
Connection.switch3[2]  = { switch4 = 1}