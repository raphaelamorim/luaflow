require "Port"

Switch = { 
            datapathId = "",
            mac = "",
            ip = "",
            alias = "",
            number_of_ports = 24,
            ports = {},
            links = {},
            state  = "off",
            type = "switch", 
         }

function Switch:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self

  --creating switch ports  
  for i = 1, self.number_of_ports do
     self.ports[i] = Port:new{number=i, switch=o}
  end
  
  return o
end