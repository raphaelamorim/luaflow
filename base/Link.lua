Link = { 
          endpoints = {},
          total_bandwidth = 1000,
          free_bandwidth = 1000,
          type = "link"
       }

function Link:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function Link:other_endpoint(current)
    for k,v in ipairs(self.endpoints) do
        if(current ~= k) then
            return k
        end
    end
end

function Link:to_s()
    s = ""
    for k,v in pairs(self.endpoints) do
       if(v.type == "port") then
          if s == "" then
              s = s .. k .. " (" .. v.number .. ")"
          else
              s = s .. "," .. k .."(" .. v.number .. ")"
          end
        else
              if s == "" then
                  s = s .. k
              else
                  s = s .. "," .. k 
              end
        end
    end
    return s
end