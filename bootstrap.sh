#!/bin/bash

basedir=$(readlink -f $(dirname $0))

cd "$basedir"
git submodule update || exit 1

cd "$basedir/deps/lua-5.2.1"
make linux || exit 1

cd "$basedir/deps/openflow"
./boot.sh || exit 1
./configure || exit 1
make || exit 1

cd "$basedir"
premake4 gmake
