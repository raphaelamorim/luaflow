function switch_ready(dpid, features)
  print(">> New switch connected: " .. dpid)
  for k,v in pairs(features) do
    if k == "ports" then
      for i,p in ipairs(v) do
        print("Port " .. i)
        for k1,v1 in pairs(p) do
          print(k1, v1)
        end
      end
    else
      print(k, v)
    end
  end
end

function packet_in(dpid, buffer_id, flow)
  print(">> New packet (" .. buffer_id .. ") received from " .. dpid)
  local idle_timeout = 10
  local out_port = "all"
  add_simple_flow(dpid, flow, buffer_id, out_port, idle_timeout)
end
