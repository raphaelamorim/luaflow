local l2_map = {}

function switch_ready(dpid, features)
  print(">> New switch connected: " .. dpid)
  l2_map[dpid] = {}
end

function packet_in(dpid, buffer_id, flow)
  print(">> New packet (" .. buffer_id .. ") received from " .. dpid)

  print("", "Learning " .. flow.dl_src .. " in port " .. flow.in_port .. " of swtich " .. dpid)
  l2_map[dpid][flow.dl_src] = flow.in_port;

  local out_port = l2_map[dpid][flow.dl_dst]

  -- Drop udp flows
  if flow.nw_proto == "udp" then
    local idle_timeout = 10
    add_drop_flow(dpid, flow, buffer_id, idle_timeout) -- Add a rule to drop the current and all next packets
    -- packet_out(dpid, flow.in_port, buffer_id) -- Use this instead to drop only the current packet
    return
  end

  if out_port == nil then
    print("", "Flooding")
    packet_out(dpid, flow.in_port, buffer_id, "all")
  else
    print("", "Routing")
    local idle_timeout = 10
    add_simple_flow(dpid, flow, buffer_id, out_port, idle_timeout)
  end
end
